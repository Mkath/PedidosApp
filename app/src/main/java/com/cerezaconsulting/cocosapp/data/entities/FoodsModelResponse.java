package com.cerezaconsulting.cocosapp.data.entities;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by kath on 5/05/18.
 */

public class FoodsModelResponse implements Serializable {
    private ArrayList<FoodsEntity> foods;

    public ArrayList<FoodsEntity> getFoods() {
        return foods;
    }

    public void setFoods(ArrayList<FoodsEntity> foods) {
        this.foods = foods;
    }
}
