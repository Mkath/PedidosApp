package com.cerezaconsulting.cocosapp.data.entities;

import java.io.Serializable;

/**
 * Created by junior on 30/09/16.
 */
public class UserEntity implements Serializable {

    private int id;
    private String userEmail;
    private String userLastName;
    private String userFirstName;
    private String password;
    private String userPhone;
    private String picture;

    public UserEntity(String email, String last_name, String first_name, String password, String phone) {
        this.userEmail = email;
        this.userLastName = last_name;
        this.userFirstName = first_name;
        this.password = password;
        this.userPhone = phone;
    }

    public String getPhone() {
        return userPhone;
    }

    public void setPhone(String phone) {
        this.userPhone = phone;
    }

    public String getLast_name() {
        return userLastName;
    }

    public void setLast_name(String last_name) {
        this.userLastName = last_name;
    }

    public String getFirst_name() {
        return userFirstName;
    }

    public void setFirst_name(String first_name) {
        this.userFirstName = first_name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return userEmail;
    }

    public void setEmail(String email) {
        this.userEmail = email;
    }

    public String getFullName(){

        return userFirstName + " " + userLastName ;
    }
}
