package com.cerezaconsulting.cocosapp.data.remote.request;

import com.cerezaconsulting.cocosapp.data.entities.CardEntity;
import com.cerezaconsulting.cocosapp.data.entities.CardRestResponse;
import com.cerezaconsulting.cocosapp.data.entities.FavouriteResponse;
import com.cerezaconsulting.cocosapp.data.entities.FoodsModelResponse;
import com.cerezaconsulting.cocosapp.data.entities.IsMyFavouriteRestaurante;
import com.cerezaconsulting.cocosapp.data.entities.RestEntinty;
import com.cerezaconsulting.cocosapp.data.entities.RestauranteResponse;
import com.cerezaconsulting.cocosapp.data.entities.StatusColorEntity;
import com.cerezaconsulting.cocosapp.data.entities.SubCatEntity;
import com.cerezaconsulting.cocosapp.data.entities.trackholder.TrackEntityHolder;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.Query;


/**
 * Created by katherine on 12/06/17.
 */

public interface ListRequest {

    @GET("foods/{foodtype}")
    Call<FoodsModelResponse> getFoods(@Header("Authorization") String token,
                                      @Path("foodtype") int foodtype);

    @GET("restaurant/RUD/{pk}/")
    Call<RestEntinty> getRestaurant(@Header("Authorization") String token, @Path("pk") int id);



    @GET("user/favourite/restaurant/")
    Call<FavouriteResponse> getMyFavouriteRestaurantes(@Header("Authorization") String token);


    @GET("user/favourite/restaurant/")
    Call<TrackEntityHolder<RestauranteResponse>> getSearchFavoritesRestaurantes(@Header("Authorization") String token,
                                                                                @Query("search") String search);


    @GET("favrestaurant/{pk}/statuscolor")
    Call<StatusColorEntity> getMyFavoriteStatus(@Header("Authorization") String token,
                                                @Path("pk") int idRestaurante);
}
