package com.cerezaconsulting.cocosapp.data.entities;

import java.io.Serializable;

/**
 * Created by kath on 5/05/18.
 */

public class FoodsEntity implements Serializable {

    private int idFood;
    private String foodName;
    private String foodImage;
    private int idFoodType;

    public String getFoodImage() {
        return foodImage;
    }

    public void setFoodImage(String foodImage) {
        this.foodImage = foodImage;
    }

    public int getIdFood() {
        return idFood;
    }

    public void setIdFood(int idFood) {
        this.idFood = idFood;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public int getIdFoodType() {
        return idFoodType;
    }

    public void setIdFoodType(int idFoodType) {
        this.idFoodType = idFoodType;
    }
}
