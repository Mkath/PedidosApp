package com.cerezaconsulting.cocosapp.presentation.main.home;

import android.annotation.SuppressLint;
import android.content.Context;

import com.cerezaconsulting.cocosapp.data.entities.AccessTokenEntity;
import com.cerezaconsulting.cocosapp.data.entities.BusServicesEntity;
import com.cerezaconsulting.cocosapp.data.entities.FoodsEntity;
import com.cerezaconsulting.cocosapp.data.entities.FoodsModelResponse;
import com.cerezaconsulting.cocosapp.data.entities.SubCatEntity;
import com.cerezaconsulting.cocosapp.data.entities.UserEntity;
import com.cerezaconsulting.cocosapp.data.entities.trackholder.TrackEntityHolder;
import com.cerezaconsulting.cocosapp.data.local.SessionManager;
import com.cerezaconsulting.cocosapp.data.remote.ServiceFactory;
import com.cerezaconsulting.cocosapp.data.remote.ServiceFactoryBusServices;
import com.cerezaconsulting.cocosapp.data.remote.request.ListRequest;
import com.cerezaconsulting.cocosapp.data.remote.request.LoginRequest;
import com.cerezaconsulting.cocosapp.data.remote.request.PostRequest;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import android.util.Base64;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by katherine on 28/06/17.
 */

public class CategoryPresenter implements CategoryContract.Presenter, SubCategoryItem {

    private CategoryContract.View mView;
    private Context context;
    private SessionManager mSessionManager;
    private boolean firstLoad = false;
    private int currentPage = 1;

    public CategoryPresenter(CategoryContract.View mView, Context context) {
        this.context = checkNotNull(context, "context cannot be null!");
        this.mView = checkNotNull(mView, "view cannot be null!");
        this.mView.setPresenter(this);
        this.mSessionManager = new SessionManager(this.context);
    }


    @Override
    public void start() {
        if (!firstLoad) {
            firstLoad = true;
            loadOrdersFromPage(1);
        }
    }


    @Override
    public void clickItem(FoodsEntity foodsEntity) {
        mView.clickItemCategories(foodsEntity);
    }

    @Override
    public void deleteItem(FoodsEntity foodsEntity, int position) {

    }

    @Override
    public void loadOrdersFromPage(int page) {
        getCategories(page);

    }

    @Override
    public void loadfromNextPage(int id) {

            getCategories(id);

    }

    @Override
    public void startLoad(int id) {
        loadOrdersFromPage(id);
    }

    @Override
    public void getCategories(final int page) {
        mView.setLoadingIndicator(true);
        ListRequest listRequest = ServiceFactory.createService(ListRequest.class);
        Call<FoodsModelResponse> orders = listRequest.getFoods("Token "+mSessionManager.getUserToken() ,page);
        orders.enqueue(new Callback<FoodsModelResponse>() {
            @Override
            public void onResponse(Call<FoodsModelResponse> call, Response<FoodsModelResponse> response) {

                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                if (response.isSuccessful()) {

                    mView.getListCategories(response.body().getFoods());


                } else {
                    mView.showErrorMessage("Error al obtener las órdenes");
                }
            }

            @Override
            public void onFailure(Call<FoodsModelResponse> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void sendFoodToBusServices(BusServicesEntity busServicesEntity) {
        mView.setLoadingIndicator(true);
        PostRequest postRequest = ServiceFactoryBusServices.createService(PostRequest.class);
        Call<Void> orders = postRequest.sendFood(mSessionManager.getUserTokenFood(),busServicesEntity);
        orders.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {

                if (!mView.isActive()) {
                    return;
                }
                if (response.isSuccessful()) {
                    mView.setLoadingIndicator(false);
                    mView.sendPedidoToBusServiceResponse();


                } else {
                    loginUser(mSessionManager.getUserEntity().getEmail(), mSessionManager.getUserPass());
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void sendEntryfoodToBusServices(BusServicesEntity busServicesEntity) {
        mView.setLoadingIndicator(true);
        PostRequest postRequest = ServiceFactoryBusServices.createService(PostRequest.class);
        Call<Void> orders = postRequest.sendEnrtyFood(mSessionManager.getUserTokenEntryFood(),busServicesEntity);
        orders.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {

                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                if (response.isSuccessful()) {

                    mView.sendPedidoToBusServiceResponse();


                } else {
                    loginUser(mSessionManager.getUserEntity().getEmail(), mSessionManager.getUserPass());
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void sendDessertToBusServices(BusServicesEntity busServicesEntity) {
        mView.setLoadingIndicator(true);
        PostRequest postRequest = ServiceFactoryBusServices.createService(PostRequest.class);
        Call<Void> orders = postRequest.sendDessert(mSessionManager.getUserTokenDessert(),busServicesEntity);
        orders.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {

                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                if (response.isSuccessful()) {

                    mView.sendPedidoToBusServiceResponse();


                } else {
                    loginUser(mSessionManager.getUserEntity().getEmail(), mSessionManager.getUserPass());

                   // mView.showErrorMessage("Error al enviar su comida");
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void sendDrinkToBusServices(BusServicesEntity busServicesEntity) {
        mView.setLoadingIndicator(true);
        PostRequest postRequest = ServiceFactoryBusServices.createService(PostRequest.class);
        Call<Void> orders = postRequest.sendDrink(mSessionManager.getUserTokenDrink(),busServicesEntity);
        orders.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {

                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                if (response.isSuccessful()) {

                    mView.sendPedidoToBusServiceResponse();


                } else {
                    loginUser(mSessionManager.getUserEntity().getEmail(), mSessionManager.getUserPass());
                    //mView.showErrorMessage("Error al enviar su comida");
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    public void loginUser(String username, String password) {
        LoginRequest loginService =
                ServiceFactory.createService(LoginRequest.class);
        Call<AccessTokenEntity> call = loginService.login(username,password);
        mView.setLoadingIndicator(true);
        call.enqueue(new Callback<AccessTokenEntity>() {
            @Override
            public void onResponse(Call<AccessTokenEntity> call, Response<AccessTokenEntity> response) {
                if(!mView.isActive()){
                    return;
                }
                if (response.isSuccessful()) {
                    openSession(response.body(), response.body().getUser());
                    mView.sendDialogConfirm(true);

                } else {
                }
            }

            @Override
            public void onFailure(Call<AccessTokenEntity> call, Throwable t) {
                if(!mView.isActive()){
                    return;
                }
            }
        });
    }

    public void openSession(AccessTokenEntity token, UserEntity userEntity) {

        mSessionManager.openSession(token);
        mSessionManager.setUser(userEntity);
    }
/*    private String generateSasToken(String uri) {

        String targetUri;
        String token = null;
        try {
            targetUri = URLEncoder
                    .encode(uri.toString().toLowerCase(), "UTF-8")
                    .toLowerCase();

            long expiresOnDate = System.currentTimeMillis();
            int expiresInMins = 60; // 1 hour
            expiresOnDate += expiresInMins * 60 * 1000;
            long expires = expiresOnDate / 1000;
            String toSign = targetUri + "\n" + expires;

            // Get an hmac_sha1 key from the raw key bytes
            byte[] keyBytes = HubSasKeyValue.getBytes("UTF-8");
            SecretKeySpec signingKey = new SecretKeySpec(keyBytes, "HmacSHA256");

            // Get an hmac_sha1 Mac instance and initialize with the signing key
            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(signingKey);

            // Compute the hmac on input data bytes
            byte[] rawHmac = mac.doFinal(toSign.getBytes("UTF-8"));

            // Using android.util.Base64 for Android Studio instead of
            // Apache commons codec
            String signature = URLEncoder.encode(
                    Base64.encodeToString(rawHmac, Base64.NO_WRAP).toString(), "UTF-8");

            // Construct authorization string
            token = "SharedAccessSignature sr=" + targetUri + "&sig="
                    + signature + "&se=" + expires + "&skn=" + HubSasKeyName;
        } catch (Exception e) {
            if (isVisible) {
                ToastNotify("Exception Generating SaS : " + e.getMessage().toString());
            }
        }

        return token;
    }*/
    private static String GetSASToken(String resourceUri, String keyName, String key)
    {
        long epoch = System.currentTimeMillis()/1000L;
        int week = 60*60*24*7;
        String expiry = Long.toString(epoch + week);

        String sasToken = null;
        try {
            String stringToSign = URLEncoder.encode(resourceUri, "UTF-8") + "\n" + expiry;
            String signature =  getHMAC256(key, stringToSign);
            sasToken = "SharedAccessSignature sig=" +
                    URLEncoder.encode(signature, "UTF-8") + "&se=" + expiry + "&skn=" + keyName + "&sr=" + URLEncoder.encode(resourceUri, "UTF-8");
        } catch (UnsupportedEncodingException e) {

            e.printStackTrace();
        }

        return sasToken;
    }

    private static String toHexString(final byte[] bytes) {
        final Formatter formatter = new Formatter();
        for (final byte b : bytes) {
            formatter.format("%02x", b);
        }
        return formatter.toString();
    }

    public static String hmacSha256(final String key, final String s) {
        String hash = null;

        try {
            final Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(new SecretKeySpec(key.getBytes(), "HmacSHA256"));
            hash = URLEncoder.encode(mac.doFinal(s.getBytes("UTF-8")).toString());

            return hash;

            //return toHexString(mac.doFinal(s.getBytes()));
        }
        catch (final Exception e) {

            return null;
            // ...
        }

    }

    public static String getHMAC256(String key, String input) {
        Mac sha256_HMAC = null;
        String hash = null;
        byte[] encodeValue;

        try {
            sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(key.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);

            hash = new String (Base64.encode(sha256_HMAC.doFinal(input.getBytes()) , Base64.DEFAULT));


            //encodeValue = Base64.encode(sha256_HMAC.doFinal(input.getBytes("UTF-8")));

            //hash = Base64..encode(sha256_HMAC.doFinal(input.getBytes("UTF-8")));

            //hash = toHexString(sha256_HMAC.doFinal(input.getBytes()));

        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }

        return hash;
    }
    private String decodeBase64(String coded){
        byte[] valueDecoded= new byte[0];
        try {
            valueDecoded = Base64.decode(coded.getBytes("UTF-8"), Base64.DEFAULT);
        } catch (UnsupportedEncodingException e) {
        }
        return new String(valueDecoded);
    }

}
