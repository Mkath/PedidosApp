package com.cerezaconsulting.cocosapp.presentation.main.home;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cerezaconsulting.cocosapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by junior on 08/06/16.
 */
public class DialogReserva extends AlertDialog {

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.question)
    TextView question;
    @BindView(R.id.tv_cancel)
    TextView tvCancel;
    @BindView(R.id.tv_aceptar)
    TextView tvAceptar;
    @BindView(R.id.ly_cancelar)
    LinearLayout lyCancelar;
    @BindView(R.id.ly_aceptar)
    LinearLayout lyAceptar;
    private String msg;

    private CategoryContract.View mView;

    public DialogReserva(CategoryContract.View mView, Context context, Bundle bundle) {
        super(context);
        LayoutInflater inflater = LayoutInflater.from(getContext());
        final View view = inflater.inflate(R.layout.dialog_confirm_reserva, null);
        setView(view);
        this.mView = mView;
        ButterKnife.bind(this, view);
        msg = bundle.getString("msg");

        question.setText(msg);
    }

    @OnClick({R.id.ly_cancelar, R.id.ly_aceptar})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ly_cancelar:
                mView.sendDialogConfirm(false);
                dismiss();
                break;
            case R.id.ly_aceptar:
                mView.sendDialogConfirm(true);
                dismiss();
                break;
        }
    }


}
